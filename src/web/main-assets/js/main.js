/* ===========================================================================
 * メイン
 * @Author
 * ========================================================================= */
(function($){$(document).ready(function(){
    
    // --------------------------------------------------------
    /**
     * 削除
     */
    $('[data-delete-key]').on(
        'click',
        function( eve ){
            var key = $(this).attr('data-delete-key');
            var target = $(this).attr('data-delete-target');
            
            
            var message = '';
            if( target == 'production' ){
                message = '本番出力結果をクリアして良いですか？';
            }
            else {
                message = 'テスト環境出力結果をクリアして良いですか？';
            }
            
            if(! confirm(message) ){
                return false;
            }
            
            $('form[data-delete-form="' + key + '"]')
                    .find('[name="target"]')
                    .val( target );
            
            $('form[data-delete-form="' + key + '"]').trigger('submit');
        }
    );
    
    // --------------------------------------------------------
    /**
     * ジェネレート
     */
    $('[data-progress-start]').on(
        'click',
        function(e){
            var key = $(this).attr( 'data-progress-start' );
            var target = $(this).attr('data-progress-target');
            var url = $(this).attr( 'href' );
            
            $('[data-progress-start="' + key + '"]').hide();
            //$('[data-progress-start="' + key + '"]').show();
            
            $('[data-progress-target="' + key + '"]' ).find('.progress-bar').css('width', '0%');
            $('[data-progress-target="' + key + '"]' ).show();
            
            $('[data-progress-message="' + key + '"]' ).show();
            
            
            callGenerate( key, target, url, 0 );
            return false;
        }
    );
    
    // --------------------------------------------------------
    /**
     * 処理する
     */
    function callGenerate( key, target, url, offset ){
        
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: {
                site_id: key,
                offset: offset,
                target: target
            }
        })
        .then(function(data){
            
            if( data.result == true ){
                $('[data-progress-message="' + key + '"] textarea' )
                            .val( data.messages.join("\n") );
                
                var rate = parseInt( ( data.currentOffset / data.contentsCount ) * 100 );
                $('[data-progress-target="' + key + '"]' ).find('.progress-bar').css('width', rate+'%');
                
                
                if( data.contentsCount <= data.currentOffset ){
                    $('[data-progress-target="' + key + '"]' ).hide();
                    $('[data-progress-target="' + key + '"]' ).find('.progress-bar').css('width', '0%');
                    $('[data-progress-message="' + key + '"]' ).hide();
                    
                    $('[data-finish-message="' + key + '"]' )
                            .empty()
                            .append( data.outputDirectory + 'に出力しました')
                            .show();
                    setTimeout(
                        function(){
                            $('[data-finish-message="' + key + '"]' ).hide();
                            $('[data-progress-start="' + key + '"]').show();
                        },
                        10000
                    )
                }
                else {
                    callGenerate( key, target, url, data.currentOffset );
                }
            }
        });
        
    }
    
    // --------------------------------------------------------
    
});})(jQuery);