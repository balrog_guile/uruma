<?php
/* ===========================================================================
 * SSGディレクトリ監視
 * @Author
 * ========================================================================= */
namespace app\libs;

use Yii;
use yii\base\Component as BaseComponent;
use yii\helpers\FileHelper;
use Michelf\Markdown;

class Direction extends BaseComponent {
    
    // 出力先
    public $outputTarget = 'production';
    
    // --------------------------------------------------------
    /**
     * サイト構成を取得
     */
    public function getSitesDatas(){
        $targetDirectory = Yii::$app->params['siteDirectory'];
        $directories = FileHelper::findDirectories( $targetDirectory, ['recursive' => false ] );
        $return = [];
        
        foreach( $directories as $dir ){
            $pathinfo = pathinfo( $dir );
            $checkPath = $dir . '/Setting.php';
            if(!file_exists($checkPath)){ continue; }
            $aliasSrc = '@app/sites/'.$pathinfo['basename'];
            if( $this->outputTarget == 'production' ){
                $aliasDest = '@app/outputs/'.$pathinfo['basename'];
            }
            else {
                $aliasDest = '@app/outputs_develop/'.$pathinfo['basename'];
            }
            $className = '\app\sites\\' . $pathinfo['basename'] . '\Setting';
            $outputDirectory = realpath( Yii::$app->params['outputDirectory'] ).DIRECTORY_SEPARATOR.$pathinfo['basename'];
            
            $return[ $className::$siteId ] = [
                'siteName' => $className::$siteName,
                'siteDirectoryName' => $pathinfo['basename'],
                'vars' => $className::$vars,
                'directory' => realpath( $dir ),
                'aliasSrc' => $aliasSrc,
                'aliasDest' => $aliasDest,
                'outputDirectory' => $outputDirectory,
                'configClassName' => $className,
                'outputExtention' => $className::$outputExtention,
                'mainLayout' => $className::$mainLayout,
                'baseUrl' => $className::$baseUrl,
                'ogp' => $className::$ogp,
                'description' => $className::$description,
            ];
            
        }
        
        return $return;
    }
    
    // --------------------------------------------------------
    /**
     * サイトIDをヴューに登録
     * @param string $site_id
     */
    public function setSiteId( $siteId ){
        Yii::$app->view->siteId = $siteId;
    }
    
    // --------------------------------------------------------
    /**
     * 出力モード選択
     */
    public function setTarget( $target ){
        $this->outputTarget = $target;
        Yii::$app->view->outputTarget = $target;
    }
    
    // --------------------------------------------------------
    /**
     * 出力先をクリア
     */
    public function deleteDest( $targetSite ){
        FileHelper::removeDirectory(
                Yii::getAlias( $targetSite['aliasDest'] )
            );
        return true;
    }
    
    
    // --------------------------------------------------------
    /**
     * サイトのファイル構造を取得
     * @param getSitesDatasで取得したデータのうち、ターゲットを絞ったもの $targetSite
     * @return array
     */
    public function getFiles( $targetSite ){
        
        ///// 除外パスなど
        $files = FileHelper::findFiles( $targetSite['directory'] );
        $configFilePath = $targetSite['directory'].DIRECTORY_SEPARATOR.'Setting.php';
        $directoryPreg = '/^'.preg_quote( $targetSite['directory'], '/' ).'/';
        $leyoutPath = $targetSite['directory'].DIRECTORY_SEPARATOR.'layouts';
        $leyoutsPreg = '/^'.preg_quote( $leyoutPath, '/' ).'/';
        $assetClassPath = $targetSite['directory'].DIRECTORY_SEPARATOR.'asset_class';
        $assetPreg = '/^'.preg_quote( $assetClassPath, '/' ).'/';
        
        
        ///// 移動対象拡張子
        $extentions = Yii::$app->params['targetExtensions'];
        $addExtension = $targetSite['configClassName']::$addExtensions;
        $return = [
            'contents' => [],
            'layouts' => [],
            'contentsCount' => 0,
        ];
        foreach( $extentions as $ex ){
            $return['contents'][ $ex ] = [];
        }
        foreach( $addExtension as $addEx ){
            $return['contents'][ $addEx ] = [];
        }
        
        ///// 対処パスの一覧を作る
        foreach( $files as $path ){
            
            // 設定ファイルパス
            if( $path == $configFilePath ){
                continue;
            }
            
            // レイアウト
            if( preg_match( $leyoutsPreg, $path ) ){
                $return['layouts'][] = $path;
                continue;
            }
            
            // アセットクラス
            if( preg_match( $assetPreg, $path ) ){
                continue;
            }
            
            
            //通常コンテンツのうち _から始まるファイルはパーツ用ファイル
            $pathinfo = pathinfo( $path );
            if(
                in_array( $pathinfo['extension'], [ 'html', 'php' ] )
                && preg_match( '/^_/', $pathinfo['filename'] )
            ){
                continue;
            }
            
            //通常コンテンツ（指定拡張子のみを処理する）
            foreach( $return['contents'] as $extention => $val ){
                if( $pathinfo['extension'] == $extention ){
                    $setPath = preg_replace( $directoryPreg, '', $path );
                    $return['contents'][ $extention ][] = $setPath;
                    if( in_array( $extention, [ 'html', 'php' ] ) ){
                        $return['contentsCount'] ++;
                    }
                }
            }
        }
        return $return;
    }
    
    // --------------------------------------------------------
    /**
     * アセットファイル処理
     * @param array $targetSite
     * @param array $siteFilePathList
     * @return array
     */
    public function assets( $targetSite, $siteFilePathList ){
        
        $return = [
            'result' => false,
            'messages' => [],
            'errorMessages' => []
        ];
        
        ///// 出力先フォルダを作成する
        $outDirectory = Yii::getAlias($targetSite['aliasDest']);
        if(! is_dir( $outDirectory ) ){
            FileHelper::createDirectory( $outDirectory, 0777, true );
            $return['messages'][] = '出力先フォルダ:' . $outDirectory . 'を作成しました';
        }
        
        ///// コンテンツ以外のファイルをコピーする
        $inDirectoryPreg = '/^'.preg_quote( $targetSite['directory'], '/' ).'/';
        foreach( $siteFilePathList['contents'] as $extention => $files ){
            if( ! in_array( $extention, [ 'html', 'php' ] ) ){
                foreach( $files as $path ){
                    
                    $srcPath = Yii::getAlias($targetSite['aliasSrc'] . $path );
                    $destPath = Yii::getAlias($targetSite['aliasDest'] . $path );
                    
                    $pathinfo = pathinfo( $destPath );
                    if(!is_dir( $pathinfo['dirname'] ) ){
                        FileHelper::createDirectory( $pathinfo['dirname'], 0777, true );
                    }
                    
                    copy( $srcPath, $destPath );
                    chmod( $destPath, 0666 );
                    $return['messages'][] = 'アセットファイル:' . $destPath . 'を作成しました';
                }
            }
        }
        
        $return['result'] = true;
        return $return;
    }
    
    // --------------------------------------------------------
    /**
     * コンテンツファイル
     * @param int $offset 何件目オフセットか
     * @param int $limit 何件処理するか
     * @param array $targetSite
     * @param array $siteFilePathList
     */
    public function contents( $offset, $limit,  $targetSite, $siteFilePathList ){
        
        $offset = (int)$offset;
        $limit = (int)$limit;
        
        Yii::$app->view->targetSite = $targetSite;
        
        $return = [
            'result' => false,
            'messages' => [],
            'errorMessages' => [],
            'currentOffset' => 0,
        ];
        
        ///// 出力先フォルダを作成する
        $outDirectory = Yii::getAlias($targetSite['aliasDest']);
        if(! is_dir( $outDirectory ) ){
            FileHelper::createDirectory( $outDirectory, 0777, true );
            $return['messages'][] = '出力先フォルダ:' . $outDirectory . 'を作成しました';
        }
        
        
        ///// コンテンツ変換
        $startCheck = 0;
        $limitCheck = 0;
        $inDirectoryPreg = '/^'.preg_quote( $targetSite['directory'], '/' ).'/';
        
        foreach( $siteFilePathList['contents'] as $extention => $files ){
            if( in_array( $extention, [ 'html', 'php' ] ) ){
                
                foreach( $files as $file ){
                    if(
                        $startCheck >= $offset
                        && $limit > $limitCheck
                    ){
                        
                        
                        // 各初期化
                        Yii::$app->controller->layout = 
                                $targetSite['aliasSrc'].'/layouts/' . $targetSite['mainLayout'].'.php';
                        Yii::$app->controller->view->subject('');
                        Yii::$app->controller->view->setDescription( $targetSite['description'] );
                        
                        $viewPath = $targetSite['aliasSrc'].$file;
                        Yii::$app->controller->view->currentRelativePath = $file;
                        
                        Yii::$app->controller->view->setOGP(
                                Yii::$app->controller->view->getCurrentUrl(),
                                $targetSite['ogp']['pageType'],
                                $targetSite['ogp']['title'],
                                $targetSite['ogp']['description'],
                                Yii::$app->controller->view->getCurrentUrl().$targetSite['ogp']['image']
                            );
                        
                        
                        
                        // 共通パラメータ
                        $params = [
                            'siteDatas' => $targetSite,
                            'vars' => $targetSite['vars'],
                        ];
                        
                        // 処理
                        $html = Yii::$app->controller->renderPartial(
                                $viewPath,
                                [
                                    $params
                                ],
                                true
                            );
                        //$html = Markdown::defaultTransform($html);
                        $parser = new \cebe\markdown\Markdown();
                        $html = $parser->parse($html);
                        $html = Yii::$app->controller->renderContent(
                                $html,
                                Yii::$app->controller->varsForLayout
                            );
                        
                        
                        $html = str_replace(
                                    [ '{{php}}', '{{/php}}', '{{=}}', '{{/=}}' ],
                                    [ '<?php ', ' ?>', '<?= ', ' ?>' ],
                                    $html
                                );
                        
                        $srcPath = Yii::getAlias($targetSite['aliasSrc'] . $file );
                        $destPath = Yii::getAlias($targetSite['aliasDest'] . $file );
                        
                        
                        if( $targetSite['outputExtention'] != 'php' ){
                            $destPath = preg_replace( '/\.php$/', '.'.$targetSite['outputExtention'], $destPath );
                        }
                        
                        
                        $pathinfo = pathinfo( $destPath );
                        if(!is_dir( $pathinfo['dirname'] ) ){
                            FileHelper::createDirectory( $pathinfo['dirname'], 0777, true );
                        }
                        file_put_contents( $destPath, $html );
                        chmod( $destPath, 0666 );
                        
                        $return['messages'][] = 'コンテンツファイル:' . $destPath . 'を作成しました';
                        
                        $limitCheck ++;
                        if( $limit == $limitCheck ){
                            $startCheck ++ ;
                            $return['currentOffset'] = $startCheck;
                            break 2;
                        }
                    }
                    $startCheck ++ ;
                    $return['currentOffset'] = $startCheck;
                }
            }
        }
        
        $return['result'] = true;
        return $return;
    }
    
    // --------------------------------------------------------
}