<?php
/* ===========================================================================
 * Viewを拡張
 * @Author
 * ========================================================================= */
namespace app\libs;

use yii\web\View as BaseView;
use Yii;
use yii\helpers\Html;
use yii\helpers\FileHelper;


class View extends BaseView{
    
    // view処理用の設定を保持
    public $targetSite;
    
    // site_id
    public $siteId;
    
    
    // タイトル
    public $subject = '';
    
    // Description
    public $description = '';
    
    // ogn
    public $ogp = [];
    
    // 実行中相対パスを保存
    public $currentRelativePath = '';
    
    // 出力モード
    public $outputTarget = 'production';
    
    
    // --------------------------------------------------------
    /**
     * 初期化
     */
    public function init() {
        parent::init();
        
        ///// プラグイン
        $preg1 = Yii::getAlias('@app/plugins');
        $preg1 = '/^'.preg_quote( $preg1, '/' ).'/';
        foreach( FileHelper::findFiles( Yii::getAlias('@app/plugins') ) as $path ){
            $pathinfo = pathinfo( $path );
            if( $pathinfo['extension'] != 'php' ){
                continue;
            }
            $relPath = preg_replace( $preg1, '', $pathinfo['dirname'] );
            $namespace = 'app\plugins' .str_replace( '/', '\\', $relPath ).'\\'.$pathinfo['filename'];
            $this->attachBehavior( $pathinfo['filename'], new $namespace() );
        }
        
    }
    
    // --------------------------------------------------------
    /**
     * レイアウトをビュー側から指定する場合
     */
    public function specicLayout( $name ){
        
        Yii::$app->controller->layout = 
                $this->targetSite['aliasSrc'].'/layouts/' . $name .'.php';
    }
    
    // --------------------------------------------------------
    /**
     * アセットの登録
     * @param string $assetName
     */
    public function setAsset( $assetName ){
        $classNamme = '\app\sites\\' . $this->targetSite['siteDirectoryName'] . '\asset_class\\' . $assetName;
        $classNamme::register( $this );
    }
    
    // --------------------------------------------------------
    /**
     * タイトルの登録
     * @param string|null タイトル
     */
    public function subject( $title = null ){
        if(!is_null($title)){
            $this->subject = $title;
        }
        return $this->subject;
    }
    
    // --------------------------------------------------------
    /**
     * パーツ呼び出し
     */
    public function partsRender( $view, $params = [] ) {
        $view = $this->targetSite['aliasSrc'] . '/'. $view;
        return Yii::$app->controller->renderPartial($view, $params, true );
    }
    
    // --------------------------------------------------------
    /**
     * ベースURL
     */
    public function getBaseUrl( $noslashend = true ){
        $return = $this->targetSite['baseUrl'][ $this->outputTarget ];
        if( $noslashend ){
            $return = preg_replace( '/\/$/', '', $return );
        }
        return $return;
    }
    
    // --------------------------------------------------------
    /**
     * ファイルパスから現在の相対パスを取得
     */
    public function getCurrentUrl(){
        
        $baseUrl = $this->getBaseUrl();
        $currentRelativePath = $this->currentRelativePath;
        $pathinfo = pathinfo( $currentRelativePath );
        if( $this->targetSite['outputExtention'] != 'php' ){
            $currentRelativePath = preg_replace(
                    '/\.php$/',
                    '.'.$this->targetSite['outputExtention'],
                    $currentRelativePath
                );
        }
        
        return $baseUrl.$currentRelativePath;
    }
    // --------------------------------------------------------
    /**
     * ディスクリプション設定
     * @param string $desc
     */
    public function setDescription( $desc ){
        $this->description = $desc;
    }
    
    // --------------------------------------------------------
    /**
     * description設定
     */
    public function getDescriptionTag(){
        $return = '<meta name="description" content="' . $this->description . '">';
        return $return;
    }
    
    // --------------------------------------------------------
    /**
     * OGP
     * @param string $url
     * @param string $pageType
     * @param string $title
     * @param string $description
     * @param string $image サイトルートからの相対
     */
    public function setOGP( $url, $pageType, $title, $description, $image ){
        $this->ogp = [
            'url' => $url,
            'pageType' => $pageType,
            'title' => $title,
            'description' => $description,
            'image' => $image,
        ];
    }
    
    // --------------------------------------------------------
    /**
     * OGP出力
     */
    public function getOGPTag(){
        $content = [];
        $content[] = '<meta property="og:url" content="' . $this->ogp['url']  . '" />';
        $content[] = '<meta property="og:type" content="' . $this->ogp['pageType']  . '" />';
        $content[] = '<meta property="og:title" content="' . $this->ogp['title']  . '" />';
        $content[] = '<meta property="og:description" content="' . $this->ogp['description']  . '" />';
        $content[] = '<meta property="og:site_name" content="' . $this->targetSite['siteName'] . '" />';
        $content[] = '<meta property="og:image" content="' . $this->ogp['image']  . '" />';
        return implode( "\n", $content );
    }
    
    // --------------------------------------------------------
    /**
     * レイアウト変更
     * @param string $name
     */
    public function setChangeLayout( $name ){
        Yii::$app->controller->layout = 
                $this->targetSite['aliasSrc'].'/layouts/' . $name.'.php';
    }
    
    // --------------------------------------------------------
    /**
     * レイアウトにわたす変数
     */
    public function setLayoutVar( $name, $val ){
        Yii::$app->controller->varsForLayout[ $name ] = $val;
    }
    
    // --------------------------------------------------------
}
