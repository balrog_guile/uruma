<?php
/* ===========================================================================
 * メインコントローラー
 * @Author
 * ========================================================================= */
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use yii\web\NotFoundHttpException;

use app\libs\Direction;

class SiteController extends Controller
{   
    public $varsForLayout = [];
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    
    // --------------------------------------------------------
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    // --------------------------------------------------------
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $Direction = new Direction();
        $sitesDirectories = $Direction->getSitesDatas();
        
        return $this->render(
                'index',
                [
                    'sitesDirectories' => $sitesDirectories,
                ]
        );
    }
    
    // --------------------------------------------------------
    /**
     * サイトを削除
     */
    public function actionDelete(){
        
        $Direction = new Direction();
        
        $site_id = Yii::$app->request->post('site_id');
        $target = Yii::$app->request->post('target');
        
        $Direction->setTarget( $target );
        $Direction->setSiteId( $site_id );
        
        $sitesDirectories = $Direction->getSitesDatas();
        
        if(! isset( $sitesDirectories[$site_id] ) ){
            throw new NotFoundHttpException(Yii::t('app', 'サイト情報がありません'));
        }
        $targetSite = $sitesDirectories[$site_id];
        $Direction->deleteDest( $targetSite );
        
        Yii::$app->session->setFlash('info', $targetSite['siteName'].'の出力先をクリアしました');
        
        return $this->redirect('/');
    }
    
    
    // --------------------------------------------------------
    /**
     * サイトジェネレート
     */
    public function actionGenerate( $site_id, $target = 'production', $offset = 0 ){
        
        $Direction = new Direction();
        
        ///// 最低限の設定
        $Direction->setTarget( $target );
        $Direction->setSiteId( $site_id );
        
        ///// サイト情報
        $sitesDirectories = $Direction->getSitesDatas();
        if(! isset( $sitesDirectories[$site_id] ) ){
            throw new NotFoundHttpException(Yii::t('app', 'サイト情報がありません'));
        }
        $targetSite = $sitesDirectories[$site_id];
        
        
        ///// サイト構成
        $siteFilePathList = $Direction->getFiles( $targetSite );
        
        ///// json
        $return = [
            'result' => false,
            'messages' => [],
            'contentsCount' => $siteFilePathList['contentsCount'],
            'currentOffset' => 0,
            'outputDirectory' => Yii::getAlias( $targetSite['aliasDest'] ),
        ];
        
        ///// オフセット0ならアセット類を処理
        if( $offset == 0 ){
            $res = $Direction->assets( $targetSite, $siteFilePathList );
            $return['result'] = $res['result'];
            $return['messages'] = array_merge( $return['messages'], $res['messages'] );
        }
        
        ///// データ変換
        $res = $Direction->contents( $offset, 100, $targetSite, $siteFilePathList );
        $return['result'] = $res['result'];
        $return['messages'] = array_merge( $return['messages'], $res['messages'] );
        $return['currentOffset'] = $res['currentOffset'];
        
        
        return json_encode( $return, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
    }
    
    // --------------------------------------------------------
    /**
     * 変数をレイアウトにわたすためのメソッド
     * @param $key
     * @param $val
     */
    public function setValues( $key, $val ){
        $this->varsForLayout[ $key ] = $val;
    }
    
    // --------------------------------------------------------
    /**
     * 出力の動作を変更
     */
    public function renderContent( $content, $vars = [] )
    {
        $layoutFile = $this->findLayoutFile($this->getView());
        $vars2 = ['content' => $content];
        $vars2 = array_merge( $vars2, $vars );
        if ($layoutFile !== false) {
            return $this->getView()->renderFile(
                    $layoutFile,
                    $vars2,
                    $this
                );
        }

        return $content;
    }
    
    // --------------------------------------------------------
}
