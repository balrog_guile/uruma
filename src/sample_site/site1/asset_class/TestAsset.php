<?php
/* ===========================================================================
 * アセットの指定テスト
 * @Author
 * ========================================================================= */
namespace app\sites\site1\asset_class;//←site1を格納フォルダ名にする
use app\assets\StaticAsset as BaseAsset;//←おまじない

class TestAsset extends BaseAsset {//TestAssetの部分をファイル名と同じにする
    
    public $css = [
    ];
    public $js = [
        'asset/js/test.js'
    ];
    
    // サイト用の依存
    public $siteDepends = [
        'TestAsset2',
    ];
    
    
}