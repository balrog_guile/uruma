<?php
/* ===========================================================================
 * アセットの指定テスト
 * @Author
 * ========================================================================= */
namespace app\sites\site1\asset_class;//←site1を格納フォルダ名にする
use app\assets\StaticAsset as BaseAsset;//←おまじない

class TestAsset2 extends BaseAsset {//TestAssetの部分をファイル名と同じにする
    
    public $css = [
    ];
    public $js = [
        'https://code.jquery.com/jquery-3.6.0.min.js',
        'asset/js/site.js'
    ];
    
    // サイト用の依存
    public $siteDepends = [
    ];
    
    
}