<?php
/* ===========================================================================
 * テストのレイアウト
 * ========================================================================= */
/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= $this->getDescriptionTag() ?>
    <?= $this->getOGPTag() ?>
    <?php $this->head() ?>
</head>
<body>
    テスト用
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>