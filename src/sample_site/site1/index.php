<?php
/* ===========================================================================
 * トップページ
 * ========================================================================= */

// レイアウト指定
$layouts = 'main';//レイアウトのファイル名から拡張子を除く　nullを指定すると単独出力

// メインレイアウトに渡したい変数を指定（このファイルの最後までに書けば有効になります）
$varsForLayout = [
    'layoutsTestVar' => 'テスト',
];

// JSなどの登録(アセット)
$this->setAsset( 'TestAsset' );

// タイトル
$this->subject( 'タイトル変更' );

//description
$this->setDescription('でぃすくりぷしょん');

//OGP
$this->setOGP(
    $this->getCurrentUrl(), //URL
    'article',//タイプ
    'テストタイトル',//タイトル
    'がいようがいよう',//概要
    $this->getBaseUrl().'/assets/img/ogp2.png'//OGP画像
);


?>
<div id="top">
# テスト
あｓｄｆさあｌｋｊｆｄ；かｌｊｆｄ；かｊ
あｓｄｆさあｌｋｊｆｄ；かｌｊｆｄ；かｊ
あｓｄｆさあｌｋｊｆｄ；かｌｊｆｄ；かｊ
<?= $this->partsRender( 'parts/_yokoyari', ['vartest'=>'変数'] ) ?>

{{php}}echo 1;{{/php}}

{{=}}'test'{{/=}}
</div>

