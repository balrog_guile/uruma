<?php
/* ===========================================================================
 * サイト情報の設定
 * ========================================================================= */
namespace app\sites\site1;//←site1を格納フォルダ名にする

use yii\base\Component as BaseComponent;//←おまじない

class Setting extends BaseComponent {
    
    ///// サイトID（他のサイトフォルダとかぶらないように設定）
    public static $siteId = 'site1';
    
    ///// BASEURL
    public static $baseUrl = [
        'develop' => 'http://sample-dev/',
        'production' => 'http://sample/',
    ];
    
    ///// サイト名
    public static $siteName = 'テストサイト1';
    
    ///// description
    public static $description = 'でふぉのでぃすくりぷしょん';
    
    ///// 変数（テンプレートに共通で渡すことができます）
    public static $vars = [
        'var1' => '変数だよ',
    ];
    
    ///// 追加する拡張子(js,html,css,scss,jpg,png,gif,svgは自動追加されます)
    public static $addExtensions = [
        'test'
    ];
    
    ///// 出力拡張子 htmlにすれば出力結果を.htmlに変換します
    public static $outputExtention = 'php';
    
    
    ///// メインのレイアウトファイル
    public static $mainLayout = 'main';
    
    
    ///// OGPのデフォルト
    public static $ogp = [
        'pageType' => 'website',
        'title' => 'テストサイト',
        'description' => '概要概要',
        'image' => '/assets/img/ogp.png',
    ];
    
    
}

