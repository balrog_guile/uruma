<?php
/* ===========================================================================
 * メイン
 * @Author
 * ========================================================================= */

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\MainAssets;
MainAssets::register( $this );

$this->title = 'StaticStitesGenerator うるま';


?>
<div class="site-index">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>サイトID</th>
                <td>サイト名</td>
                <td></td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach( $sitesDirectories as $key => $datas ): ?>
            <tr>
                <th><?= $key ?></th>
                <td><?= $datas['siteName'] ?></td>
                <td>
                    <?= Html::beginForm(
                            ['/site/delete'],
                            'post',
                            [
                                'data-delete-form' => $key
                            ]
                        )
                    ?>
                    <?= Html::hiddenInput( 'site_id', $key ) ?>
                    <?= Html::hiddenInput( 'target', 'production' ) ?>
                    <?=
                        Html::button(
                            '<i class="fa fa-hand-o-down" aria-hidden="true"></i> 本番出力結果をクリア',
                            [
                                'class' => 'btn btn-info',
                                'data-delete-key' => $key,
                                'data-delete-target' => 'production'
                            ]
                        )
                    ?>
                    <?=
                        Html::button(
                            '<i class="fa fa-hand-o-down" aria-hidden="true"></i> テスト環境出力結果をクリア',
                            [
                                'class' => 'btn btn-info',
                                'data-delete-key' => $key,
                                'data-delete-target' => 'develop'
                            ]
                        )
                    ?>
                    <?= Html::endForm() ?>
                </td>
                <td>
                    <?=
                        Html::a(
                            '<i class="fa fa-fighter-jet" aria-hidden="true"></i> 本番をジェネレート！',
                            //['/site/generate', 'site_id' => $key ],
                            ['/site/generate' ],
                            [
                                'class' => 'btn btn-success',
                                'data-progress-start' => $key,
                                'data-progress-target' => 'production',
                            ]
                        )
                    ?>
                    <?=
                        Html::a(
                            '<i class="fa fa-ship" aria-hidden="true"></i> テスト環境をジェネレート！',
                            //['/site/generate', 'site_id' => $key ],
                            ['/site/generate' ],
                            [
                                'class' => 'btn btn-success',
                                'data-progress-start' => $key,
                                'data-progress-target' => 'develop',
                            ]
                        )
                    ?>
                    <div class="progress" data-progress-target="<?= $key ?>" style="display: none;">
                        <div class="progress-bar" role="progressbar" style="width: 60%;">
                        </div>
                    </div>
                    <div style="display: none;" data-progress-message="<?= $key ?>">
                        <textarea class="form-control"></textarea>
                    </div>
                    <div style="display: none;" class="alert alert-success" role="alert" data-finish-message="<?= $key ?>">
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
