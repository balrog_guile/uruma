<?php
/* ===========================================================================
 * メインアセット
 * @Author
 * ========================================================================= */
namespace app\assets;

use yii\web\AssetBundle;
use Yii;

class StaticAsset extends AssetBundle
{
    public $basePath = '';
    public $baseUrl = '';
    public $depends = [];
    
    // サイト用の依存
    public $siteDepends = [];
    
    // --------------------------------------------------------
    /**
     * 初期化
     */
    public function init() {
        parent::init();
        $dicrectory = Yii::$app->controller->view->targetSite['siteDirectoryName'];
        foreach( $this->siteDepends as $one ){
            $classNamme = 'app\sites\\' . $dicrectory . '\asset_class\\' . $one;
            $this->depends[] = $classNamme;
        }
        
    }
    
    // --------------------------------------------------------
    
}
