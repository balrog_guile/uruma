<?php
/* ===========================================================================
 * メインアセット
 * @Author
 * ========================================================================= */
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MainAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'main-assets/js/main.js',
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
